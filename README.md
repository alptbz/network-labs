# Beispiele für Netzwerk-Labore

- [Cisco Basic](./labs/cisco-basic/README.md)
- [Cisco QoS](./labs/cisco-qos/README.md)
- [Cisco QSPF](./labs/cisco-ospf/README.md)
- [MikroTik Basic](./labs/mikrotik-basic/README.md)
- [MikroTik QoS](./labs/mikrotik-qos/README.md)
- [MikroTik OSPF](./labs/mikrotik-ospf/README.md)

# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.