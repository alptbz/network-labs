# MikroTik OSPF - Example configuration

# Usefull links
 - https://help.mikrotik.com/docs/display/ROS/OSPF
 - https://help.mikrotik.com/docs/display/ROS/Route+Selection+and+Filters


# Lab
![Lab topology](Lab.png)

# Goals
 - Routes leading to a different area should to be summarized
 - Within the OSPF network, internal subnets should not be propagated as external routes
 

# ZH-R1
 - ASBR (Connected to external networks)

```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.1.10.129/32
# This filter prevents the advertisement of "opsf internal" subnet 10.1.10.0/24 by connected routes
# This is only needed, because redistribute=connected is set
/routing filter rule add chain=ospf-out disabled=no rule="if (protocol connected && dst in 10.1.10.0/24) { reject } else { accept }"
/routing ospf instance add disabled=no redistribute=connected out-filter-chain=ospf-out name=default router-id=10.1.10.129
/routing ospf area add disabled=no instance=default name=backbone area-id=0.0.0.0
/routing ospf interface-template add area=backbone disabled=no interfaces=loopback,ether2,ether3,ether4 networks=10.1.10.128/25,10.1.10.0/25
/system identity/set name=ZH-R1
/ip address/add interface=ether2 address=10.1.10.1/30
/ip address/add interface=ether3 address=10.1.10.5/30
/ip address/add interface=ether4 address=10.1.10.9/30

# Management network for lab
/ip dhcp-client add interface=ether1

# Internet access for lab
/ip dhcp-client add interface=ether5
```

# ZH-R2
 - ABR between Area `backbone` and `bs` 

```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.1.10.130/32
/routing ospf instance add disabled=no name=default router-id=10.1.10.130
/routing ospf area add disabled=no instance=default name=backbone area-id=0.0.0.0
/routing ospf area add area-id=0.0.0.1 disabled=no instance=default name=bs
/routing ospf interface-template add area=backbone disabled=no interfaces=loopback,ether2,ether3,ether4 networks=10.1.10.128/25,10.1.10.0/25
/routing ospf area range add area=backbone disabled=no prefix=10.1.10.0/24
/routing ospf area range add area=bs disabled=no prefix=10.2.20.0/24
/system identity/set name=ZH-R2
/ip address/add interface=ether2 address=10.1.10.2/30
/ip address/add interface=ether3 address=10.1.10.13/30
/ip address/add interface=ether4 address=10.1.10.17/30
/ip address/add interface=ether8 address=10.2.20.61/30
```

# ZH-R3
```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.1.10.131/32
/routing ospf instance add disabled=no name=default router-id=10.1.10.131
/routing ospf area add disabled=no instance=default name=backbone area-id=0.0.0.0
/routing ospf interface-template add area=backbone disabled=no interfaces=loopback,ether2,ether3 networks=10.1.10.128/25,10.1.10.0/25
/system identity/set name=ZH-R3
/ip address/add interface=ether2 address=10.1.10.6/30
/ip address/add interface=ether3 address=10.1.10.14/30
```

# ZH-R4
```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.1.10.132/32
/routing ospf instance add disabled=no name=default router-id=10.1.10.132
/routing ospf area add disabled=no instance=default name=backbone area-id=0.0.0.0
/routing ospf interface-template add area=backbone disabled=no interfaces=loopback,ether2,ether3 networks=10.1.10.128/25,10.1.10.0/25
/system identity/set name=ZH-R4
/ip address/add interface=ether2 address=10.1.10.10/30
/ip address/add interface=ether3 address=10.1.10.18/30
```

# BS-1
```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.2.20.129/32
/routing ospf instance add disabled=no name=default router-id=10.2.20.129
/routing ospf area add disabled=no instance=default name=bs area-id=0.0.0.1
/routing ospf interface-template add area=bs disabled=no interfaces=loopback,ether2,ether3,ether8 networks=10.2.20.128/25,10.2.20.0/25
/system identity/set name=BS-R1
/ip address/add interface=ether2 address=10.2.20.1/30
/ip address/add interface=ether3 address=10.2.20.5/30
/ip address/add interface=ether8 address=10.2.20.62/30 

```

# BS-2
```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.2.20.130/32
/routing ospf instance add disabled=no name=default router-id=10.2.20.130
/routing ospf area add disabled=no instance=default name=bs area-id=0.0.0.1
/routing ospf interface-template add area=bs disabled=no interfaces=loopback,ether2,ether3 networks=10.2.20.128/25,10.2.20.0/25
/system identity/set name=BS-R2
/ip address/add interface=ether2 address=10.2.20.2/30
/ip address/add interface=ether3 address=10.2.20.9/30
```

# BS-3
```routeros
/interface/bridge/add name=loopback
/ip address/add interface=loopback address=10.2.20.131/32
/routing ospf instance add disabled=no name=default router-id=10.2.20.131
/routing ospf area add disabled=no instance=default name=bs area-id=0.0.0.1
/routing ospf interface-template add area=bs disabled=no interfaces=loopback,ether2,ether3 networks=10.2.20.128/25,10.2.20.0/25
/system identity/set name=BS-R3
/ip address/add interface=ether2 address=10.2.20.6/30
/ip address/add interface=ether3 address=10.2.20.10/30
```

# From static to dynamic routing
If static IPv4 routing is functioning within the network, configuring OSPF can be accomplished with ease:
 - Define router-ids for all routers
 - Add Lookback interface with router-id as IP
 - Configure OSPF (instance, area, interface-template, filters, ...)
 - Check if OSPF routes are beeing installed correctly (Check LSA, neighbours and routing table)
 - Disable static routes
 - Test it

# Observations

## Optimal routing table

### BS-R2
#### LSA DB (Bad)
contains uneseccary LSAs
![BS-R2 Lab LSA Bad](BS-R2-LSA-Bad.PNG)
#### Routing Table (Bad)
Routing entries are not summarized
![BS-R2 Lab Routing Bad](BS-R2-Routes-Bad.PNG)

### Identified problems: 
 - `inter-area-prefixes` are also advertised as `external`
 - There is a summarized route (10.1.10.0/24), but all the smaller subnets are unnecessarily leaking through the ABR, leading to an increase in the routing table size.

### Cause: 
 - `ZH-R1` has `redistribute=connected` enabled, which allows it to redistribute the connected routes learned from ether1 and ether5. However, this setting also inadvertently leads the router to distribute routes on ether2, ether3, and ether4. This is redundant because these routes are already disseminated as `network` Link-State Advertisements (LSAs) because they are part of the OSPF area.
 - Route summarization is missing on ZH-R2 (ABR)

### Solution:
 - Filter `10.1.10.0/24` from connect in OSPF Out on `ZH-R1`
 - Configure `area ranges` on `ZH-R2` for summarization

#### LSA DB (Good)
no redundant LSA entries
![BS-R2 Lab LSA Good](BS-R2-LSA-Good.PNG)

#### Routing Table (Good)
inter-area routes are summarized
![BS-R2 Lab Routing Good](BS-R2-Routes-Good.PNG)
