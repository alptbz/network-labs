# Cisco QOS Lab

![Network Lab Overview](media/overview.PNG)

# Goal
Demonstrate the use of policies to prioritize RTP stream with Cisco Routers

# Hardware
 - 2 x Cisco IOS Router mind. V12.3
 - 4 x PC with Ubuntu

If a Serial interface capable of approximately 10 Mbit/s is not available, utilize any available Ethernet interface and manually configure the interface to operate in 10-BaseT mode. **Reason:** It facilitates the experiment. Utilizing fewer resources generates less traffic, efficiently occupying the available bandwidth when it is limited.

# Internet Uplink
For easier labbing (for example installation of packages on ubuntu machines) extend the network and add an uplink on R1.

# Preparations
 - Reset Configuartion on R1 and R2
 - Enable required interfaces *Hint:* `no shutdown`
 - Configure all IP interfaces on Cisco Router
<br>Example for Serial 0/1/0 on R1:
```
interface Serial0/1/0
 ip address 192.168.80.1 255.255.255.252
```
 - Install Ubuntu on all PCs, Configure Hostname
 - Configure IPs on all PCs
 - Install required packages: `apt install vim net-tools wireshark vlc iperf3`

# Experiment 1 - iperf without configuration

## Considerations
 - Utilize UDP for bandwidth exhaustion, as TCP will only consume the available bandwidth, preventing the forced packet drops in the router we seek to achieve.

## Steps
 - On PC81-1 and PC81-2 start `iperf3` server
```
iperf3 -s
```
 - On PC79-1 and PC79-2 start `iperf3` client sessions:
```bash
iperf3 -c 192.168.81.2 -t 120 -u -b 10M
# For bandwidth, utilize the entire available link capacity between the routers.
# Initiate the iperf3 client on both clients simultaneously.
```

 - How much packages are lost by each test?
 - Is the bandwith equally distributed?

# Experiment 2 - iperf with dscp

Repeat Experiment 1 but manually set the DSCP value on one of the "iperf3 clients":

```bash
iperf3 -c 192.168.81.2 -t 120 -u -b 10M --dscp 38
# For bandwidth, utilize the entire available link capacity between the routers.
# Initiate the iperf3 client on both clients simultaneously.
```

See [Cisco - Implement Quality of Service Policies with DSCP](https://www.cisco.com/c/en/us/support/docs/quality-of-service-qos/qos-packet-marking/10103-dscpvalues.html) for more information about DSCP values. 

 - What changed in comparison to experiment 2?

Try to locate the DSCP value within the IP header with Wireshark.
 - What is the default DSCP value used by the client with no DSCP set?

# Experiment 3 - iperf with dscp and policy-maps

Cisco routers will enforce QoS rules only when properly configured. Attempt to set up policy-map settings on R1 by yourself.

 - How much bandwidth needs to be allocated to one of the iperf3 tests to observe a noticeable change during the tests?

example cisco ios configuration:
```
class-map match-any DSCPAF43CLASS
  match ip dscp af43

policy-map DSCPAF43POLICY
  class DSCPAF43CLASS
    priority 7000

interface Serial0/1/0
bandwidth 8000
max-reserved-bandwidth 100
service-policy output DSCPAF43POLICY
```
## Ressources
 - [Ascertain Bandwidth and Priority Commands of a QoS Service Policy](https://www.cisco.com/c/en/us/support/docs/quality-of-service-qos/qos-packet-marking/10100-priorityvsbw.html)
 - [Cisco - Implement Quality of Service Policies with DSCP](https://www.cisco.com/c/en/us/support/docs/quality-of-service-qos/qos-packet-marking/10103-dscpvalues.html)

# Experiment 4 - vlc + iperf

Implementing QoS within networks becomes indispensable, especially when dealing with real-time, low-latency applications such as Voice over IP. It ensures that these applications receive priority, maintaining seamless operation and optimal user experience. Meanwhile, other applications, like video streaming services, buffer their videos, hence removing the real-time requirement.

One way to experience this in a lab enviroment is by streaming a video. 

Use to machines (for example PC79-1 and PC81-1) to stream a video with vlc while flooding the connection between the routers using the other two machines. 

## Considerations
 - The streamed video should consume at least 60% of the available bandwidth. Modify the resolution and verify the amount of bandwidth utilized.
 - If the stream is larger than the available bandwidth, the receiving VLC client will not display any video, as there isn't sufficient data available to assemble the picture.

## Command examples
 - Use VLC to transcode and stream a video using RTP (UDP) unicast to another host
```bash 
vlc Aquarium.webm --sout="#transcode{vcodec=hevc,scale=Auto,width=1440,height=810,acodec=mpga,ab=128,channels=2,samplerate=44100,scodec=none}:rtp{dst=192.168.81.2,port=5004,mux=ts,sap,name=Stream}"
```
 - Use VLC to receive a unicast RTP stream
```bash
vlc rtp://@:5004
```
 - Policy Map for RTP stream on Cisco Router:
```
class-map match-all VIDEO
  match ip rtp 5004 16383

policy-map VIDEO
  class VIDEO
  priority 7000

interface Serial0/1/0
  service-policy output VIDEO
```

