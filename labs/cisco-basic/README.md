# Cisco Basic Configuration

**Do not forget to save your configuration**<br>
`write memory`

# Common used show commands
 - `show interfaces summary`
 - `show ip interfaces brief`
 - `show ip route`


# Set IP Address on interface

### Configuration excerpt
```
interface FastEthernet0/0
 ip address 192.168.81.1 255.255.255.128
```
### CLI
 - `conf t`
 - `int f 0/0`
 - `ip address 192.168.81.1 255.255.255.128`
 - `no shutdown`
 - `end`


# Add default route 

### Configuration excerpt

```
ip route 0.0.0.0 0.0.0.0 192.168.10.1
```

### CLI
 - `conf t`
 - `ip route 0.0.0.0 0.0.0.0 192.168.10.1`


# Set Hostname
### Configuration excerpt
```
hostname R1
```

### CLI
 - `conf t`
 - `hostname R3`
 - `end`

# Set Password and enable telnet

### Configuration excerpt
```
username admin password cisco
enable secret cisco
line vty 0 4
  password cisco
  transport input all
  login
line vty 5 15
  password cisco
  transport input all
  login  
```

### CLI
 - `conf t`
 - `username admin password cisco`
 - `enable secret cisco`
 - `line vty 0 15`
 - `transport input all`
 - `password cisco`
 - `login`
 - `end`

# SNMP

### Configuration excerpt
```
snmp-server community public RO
```

### CLI
 - `conf t`
 - `snmp-server community public RO`
 - `end`


