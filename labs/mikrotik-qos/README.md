# MikroTik QoS 

Based on the [Cisco QOS Lab](../cisco-qos/README.md), but instead of using two Cisco Routers two MikroTik Routers are used. 

MikroTik implements QoS by defining `Queues` and assining bandwith to them. 



## Example configuration

```mikrotik
/ip firewall mangle
add action=mark-connection chain=forward dst-port=5200 in-interface=bridge_79_130 new-connection-mark=5200 passthrough=yes protocol=udp
add action=mark-packet chain=forward connection-mark=5200 new-packet-mark=medium-priority passthrough=yes
add action=mark-connection chain=forward dst-port=22 new-connection-mark=ssh passthrough=yes protocol=tcp
add action=mark-packet chain=forward connection-mark=ssh new-packet-mark=high-priority passthrough=yes
add action=mark-connection chain=forward dst-port=8291 new-connection-mark=winbox passthrough=yes protocol=tcp
add action=mark-packet chain=prerouting connection-mark=winbox new-packet-mark=high-priority passthrough=ye

/queue tree
add name=queue-ether3 parent=ether3
add limit-at=1M max-limit=10M name=queue-high-priority packet-mark=high-priority parent=queue-ether3 priority=2
add name=queue-low packet-mark=no-mark parent=queue-ether3
add limit-at=7M max-limit=10M name=queue-medium packet-mark=medium-priority parent=queue-ether3 priority=3
```

## Ressources
 - [MikroTik Quality of Service Presentation Aris Vink](https://mum.mikrotik.com/presentations/NL19/presentation_6713_1556529224.pdf)
 - [MikroTik QoS: simple queues](https://www.youtube.com/watch?v=tnzxrt6bgbs)