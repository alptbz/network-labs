# MikroTik Basic Commands

See [Command Line Reference](https://help.mikrotik.com/docs/display/ROS/Command+Line+Interface) on MikroTik Website for Basics

**Always replace example values in commands with the desired values**

# IPv4-Adresse konfigurieren

 - Editing and removing entries is only explained for ipv4 addresses. It's the same logic everywhere. 

## CLI
 - define **new** IPv4 for interface (replace ether1 and 192.168.1.1/24 with your values)
```routeros
/ip address add interface=ether1 address=192.168.1.1/24
```
 - edit **existing** IPv4 for interface (replace ether1 and 192.168.1.1/24 with your values)
```routeros
# First print to get number 
/ip address print
# Edit entry
/ip address set numbers=1 address=192.168.2.1/24
```

# IPv4-Route konfigurieren
## CLI
 - define **new** route
```routeros
/ip route add gateway=192.168.1.1 dst-address=192.168.10.0/24
```

- define **new** default route
```routeros
/ip route add gateway=192.168.1.1 dst-address=0.0.0.0/0
```

# Set Hostname (Identity)

## CLI
```routeros
/system identity set name=WRZH-R1
```

# Reset configuration

## CLI
 - It is recommended to reset the router before using and removing the default konfiguration
```routeros
/system reset-configuration keep-users=no no-defaults=yes skip-backup=yes
```
## Hardware
 - Disconnect the power plug from the device 
 - Press and keep holding the reset button 
 - Connect the power plug to the device
 - Keep holding the reset button **until** one of the led blinks for 5 times

# ping

ICMP echo requests

## CLI
```routeros
/ping 192.168.23.1
```

Example output of a successful ping with a round-trip time (RTT) of an average of 457 µs:
```routeros
[admin@ZH-R1] > /ping 192.168.23.1
  SEQ HOST                                     SIZE TTL TIME       STATUS       
    0 192.168.23.1                               56  64 497us     
    1 192.168.23.1                               56  64 475us     
    2 192.168.23.1                               56  64 414us     
    3 192.168.23.1                               56  64 445us     
    sent=4 received=4 packet-loss=0% min-rtt=414us avg-rtt=457us max-rtt=497us 
```

example output of an unsuccessful ping (host seams unreachable and is not answering to the echo requests):
```routeros
[admin@ZH-R1] > /ping 192.168.23.2
  SEQ HOST                                     SIZE TTL TIME       STATUS       
    0 192.168.23.2                                                 timeout      
    1 192.168.23.2                                                 timeout      
    2 192.168.23.2                                                 timeout      
    3 192.168.23.142                             84  64 86ms630us  host unrea...
    4 192.168.23.2                                                 timeout      
    5 192.168.23.2                                                 timeout      
    sent=6 received=0 packet-loss=100% 
```

# traceroute

ICMP echo request with an increasing TTL until the host is reached.

## CLI
```routeros
/ping 192.168.23.1
```

successful traceroute:

```routeros
[admin@ZH-R1] > /tool/traceroute 10.2.20.130
Columns: ADDRESS, LOSS, SENT, LAST, AVG, BEST, WORST, STD-DEV
#  ADDRESS      LOSS  SENT  LAST   AVG  BEST  WORST  STD-DEV
1  10.1.10.2    0%       7  0.4ms  0.5  0.3   0.7    0.1    
2  10.2.20.62   0%       7  0.6ms  0.8  0.4   1.1    0.2    
3  10.2.20.130  0%       7  0.8ms  1    0.6   1.5    0.3    
```

Successful traceroute, but some hops along the way are not answering:

```routeros
[admin@ZH-R1] > /tool/traceroute 10.2.20.130
Columns: ADDRESS, LOSS, SENT, LAST, AVG, BEST, WORST, STD-DEV
#  ADDRESS      LOSS  SENT  LAST     AVG  BEST  WORST  STD-DEV
1  10.1.10.2    0%      10  0.5ms    0.4  0.3   0.5    0.1    
2               100%    10  timeout                           
3  10.2.20.130  0%       9  1.8ms    1.6  1.2   1.8    0.2    
```

Successful traceroute using reverse dns to identity the hosts:

```routeros
[admin@ZH-R2] > /tool/traceroute 9.9.9.9 use-dns=yes 
Columns: ADDRESS, LOSS, SENT, LAST, AVG, BEST, WORST, STD-DEV
 #  ADDRESS                          LOSS  SENT  LAST   AVG  BEST  WORST  STD-DEV
 1  10.1.10.1                        0%       5  0.7ms  0.7  0.5   0.7    0.1    
 2  192.168.122.1                    0%       5  0.8ms  0.8  0.7   0.9    0.1    
 3  10.0.33.1                        0%       5  2ms    2    2     2.1    0      
 4  10.0.40.254                      0%       5  1ms    0.9  0.8   1      0.1    
 5  r1.tbszuerich.fiber7.init7.net.  0%       5  1ms    0.9  0.8   1      0.1    
 6  r1.790lim.fiber7.init7.net.      0%       5  1.2ms  1.1  1     1.2    0.1    
 7  r1zrh16.core.init7.net.          0%       5  1.7ms  1.9  1.7   2.1    0.2    
 8  r1zrh2.core.init7.net.           0%       5  1.9ms  1.8  1.6   1.9    0.1    
 9  r1zrh5.core.init7.net.           0%       5  1.8ms  1.8  1.7   1.9    0.1    
10  194.42.48.46                     0%       5  1.3ms  1.1  1     1.3    0.1    
11  dns9.quad9.net.                  0%       5  1.2ms  1.2  1.1   1.3    0.1    
```

# Netwatch

Instead of pinging the hosts in the terminal over and over, *netwatch* can be used to continuously monitor one or more hosts.

## CLI - Add a host

ping the host every 10 seconds:

```routeros
/tool/netwatch/add host=9.9.9.9 type=icmp interval=10s
```

## CLI - Monitor

Example output of netwatch status, showing that the last ping to 9.9.9.9 was successful and the host is considered up.

```routeros
[admin@ZH-R2] > /tool/netwatch/print 
Columns: TYPE, HOST, INTERVAL, STATUS, SINCE
# TYPE  HOST     INTERVAL  STATUS  SINCE               
0 icmp  9.9.9.9  10s       up      oct/22/2023 08:32:12
```

## Winbox - Monitor

![Netwatch](netwatch.PNG)