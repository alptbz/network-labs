# Cisco OSPF Basic configuration

## Ressourcen
 - [Cisco - Understand Open Shortest Path First (OSPF) - Design Guide](https://www.cisco.com/c/en/us/support/docs/ip/open-shortest-path-first-ospf/7039-1.html)
 - [Cisco OSPF Areas Tutorial](https://www.flackbox.com/cisco-ospf-areas)

## Minimale OSPF Konfiguration

### Topology 
```
R1 <-> R2 <-> R3
```

### R1
```cisco
interface Loopback0
 ip address 10.255.254.1 255.255.255.255
 ip ospf 1 area 0
!!
interface GigabitEthernet0/1
 ip address 10.2.255.1 255.255.255.252
 ip ospf 1 area 0

router ospf 1
 router-id 10.255.254.1
 network 10.2.0.0 0.0.255.255 area 0
 network 10.255.254.0 0.0.0.255 area 0
```


### R2
```cisco
interface Loopback0
 ip address 10.255.254.2 255.255.255.255
 ip ospf 1 area 0
!
interface GigabitEthernet0/1
 ip address 10.2.255.2 255.255.255.252
 ip ospf 1 area 0
!
interface GigabitEthernet0/2
 ip address 10.2.255.5 255.255.255.252
 ip ospf 1 area 0
!
router ospf 1
 router-id 10.255.254.2
 network 10.2.0.0 0.0.255.255 area 0
 network 10.255.254.0 0.0.0.255 area 0
```

### R3
```cisco
interface Loopback0
 ip address 10.255.254.3 255.255.255.255
 ip ospf 1 area 0
!
interface GigabitEthernet0/1
 ip address 10.2.255.6 255.255.255.252
 ip ospf 1 area 0
!
router ospf 1
 router-id 10.255.254.3
 network 10.2.0.0 0.0.255.255 area 0
 network 10.255.254.0 0.0.0.255 area 0
```

## Route Summarization auf ABR

```
router ospf 1
 router-id 10.255.254.3
 area 1 range 10.10.0.0 255.255.0.0
 network 10.2.0.0 0.0.255.255 area 1
 network 10.0.0.0 0.0.255.255 area 0
```

